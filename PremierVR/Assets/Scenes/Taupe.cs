﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Taupe : MonoBehaviour
{

    public GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TaupeStay());
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator TaupeStay()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }

    public void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            StopCoroutine(TaupeStay());
            Destroy(gameObject);
            gameManager.point++;

        }

    }
}
