﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject taupe;
    public List<Spawner> spawn;
    public int point;
    public TextMesh pointsDisplay;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CreateTaupe());

    }

    // Update is called once per frame
    void Update()
    {
        pointsDisplay.text = "Points  : " + point;
    }

    IEnumerator CreateTaupe()
    {
        while (true)
        {
            int ran = Random.Range(0, spawn.Count);
            while (spawn[ran].taupeIsIn) {
                ran = Random.Range(0, spawn.Count);
            }
            Instantiate(taupe, spawn[ran].transform.position, Quaternion.identity);
            float ranf = Random.Range(0.25f,1f);
            yield return new WaitForSeconds(ranf);

        }
        
    }
}
